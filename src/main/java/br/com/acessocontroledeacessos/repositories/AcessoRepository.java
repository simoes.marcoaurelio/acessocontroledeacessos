package br.com.acessocontroledeacessos.repositories;

import br.com.acessocontroledeacessos.models.Acesso;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {
    Acesso findByClienteIdAndPortaId(int clienteId, int portaId);
}

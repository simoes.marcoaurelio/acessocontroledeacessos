package br.com.acessocontroledeacessos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AcessoControleDeAcessosApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcessoControleDeAcessosApplication.class, args);
	}
}

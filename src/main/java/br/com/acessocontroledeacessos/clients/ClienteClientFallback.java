package br.com.acessocontroledeacessos.clients;

import br.com.acessocontroledeacessos.dtos.Cliente;
import br.com.acessocontroledeacessos.exceptions.ServicoClienteIndisponivelException;

public class ClienteClientFallback implements ClienteClient{

    @Override
    public Cliente consultarClientePorId(int id) {
        throw new ServicoClienteIndisponivelException();
    }
}

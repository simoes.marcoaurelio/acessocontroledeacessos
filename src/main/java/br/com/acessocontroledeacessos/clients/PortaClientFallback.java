package br.com.acessocontroledeacessos.clients;

import br.com.acessocontroledeacessos.dtos.Porta;
import br.com.acessocontroledeacessos.exceptions.ServicoPortaIndisponivelException;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta consultarPortaPorId(int id) {
        throw new ServicoPortaIndisponivelException();
    }
}

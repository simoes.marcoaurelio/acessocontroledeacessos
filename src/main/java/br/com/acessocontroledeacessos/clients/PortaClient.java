package br.com.acessocontroledeacessos.clients;

import br.com.acessocontroledeacessos.dtos.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta", configuration = PortaClientConfiguration.class)
public interface PortaClient {

    @GetMapping("/porta/{id}")
    Porta consultarPortaPorId(@PathVariable int id);
}

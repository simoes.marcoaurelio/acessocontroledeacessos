package br.com.acessocontroledeacessos.clients;

import br.com.acessocontroledeacessos.dtos.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    Cliente consultarClientePorId(@PathVariable int id);
}

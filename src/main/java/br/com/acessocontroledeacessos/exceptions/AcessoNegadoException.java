package br.com.acessocontroledeacessos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Acesso informado não encontrado no sistema")
public class AcessoNegadoException extends RuntimeException {
}

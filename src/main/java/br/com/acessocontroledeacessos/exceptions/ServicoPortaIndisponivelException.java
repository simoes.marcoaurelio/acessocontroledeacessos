package br.com.acessocontroledeacessos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Serviço de consulta a porta indisponível")
public class ServicoPortaIndisponivelException extends RuntimeException {
}

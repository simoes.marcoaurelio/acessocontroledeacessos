package br.com.acessocontroledeacessos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta informada não encontrada no sistema")
public class PortaNaoEncontradaException extends RuntimeException{
}

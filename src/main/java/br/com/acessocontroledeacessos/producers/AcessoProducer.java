package br.com.acessocontroledeacessos.producers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, LogAcesso> producer;

    public void enviarAoKafka(LogAcesso logAcesso) {
        producer.send("spec3-marco-aurelio-1", logAcesso);
    }
}

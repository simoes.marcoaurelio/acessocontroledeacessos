package br.com.acessocontroledeacessos.controllers;

import br.com.acessocontroledeacessos.dtos.AcessoMapper;
import br.com.acessocontroledeacessos.dtos.CadastrarOuConsultarAcesso;
import br.com.acessocontroledeacessos.models.Acesso;
import br.com.acessocontroledeacessos.producers.LogAcesso;
import br.com.acessocontroledeacessos.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoMapper acessoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CadastrarOuConsultarAcesso cadastrarAcesso(
            @RequestBody @Valid CadastrarOuConsultarAcesso cadastrarOuConsultarAcesso) {

        Acesso acesso = acessoMapper.paraAcesso(cadastrarOuConsultarAcesso);
        acesso = acessoService.cadastrarAcesso(acesso);
        return acessoMapper.paraCadastrarOuConsultarAcesso(acesso);
    }

    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAcesso(@PathVariable int cliente_id, @PathVariable int porta_id) {
        acessoService.deletarAcesso(cliente_id, porta_id);
    }

    @GetMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.OK)
    public CadastrarOuConsultarAcesso consultarAcesso(@PathVariable int cliente_id, @PathVariable int porta_id) {

        Acesso acesso = acessoService.consultarAcesso(cliente_id, porta_id);

        LogAcesso logAcesso = acessoService.registrarLogDeAcesso(acesso, cliente_id, porta_id);

        acessoService.tratarAcessoNegado(acesso);

        return acessoMapper.paraCadastrarOuConsultarAcesso(acesso);
    }
}

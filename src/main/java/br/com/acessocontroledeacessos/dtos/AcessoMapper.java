package br.com.acessocontroledeacessos.dtos;

import br.com.acessocontroledeacessos.models.Acesso;
import org.springframework.stereotype.Component;

@Component
public class AcessoMapper {

    public Acesso paraAcesso(CadastrarOuConsultarAcesso cadastrarOuConsultarAcesso) {
        Acesso acesso = new Acesso();

        acesso.setPortaId(cadastrarOuConsultarAcesso.getPortaId());
        acesso.setClienteId(cadastrarOuConsultarAcesso.getClienteId());

        return acesso;
    }

    public CadastrarOuConsultarAcesso paraCadastrarOuConsultarAcesso(Acesso acesso) {
        CadastrarOuConsultarAcesso cadastrarOuConsultarAcesso = new CadastrarOuConsultarAcesso();

        cadastrarOuConsultarAcesso.setPortaId(acesso.getPortaId());
        cadastrarOuConsultarAcesso.setClienteId(acesso.getClienteId());

        return cadastrarOuConsultarAcesso;
    }
}

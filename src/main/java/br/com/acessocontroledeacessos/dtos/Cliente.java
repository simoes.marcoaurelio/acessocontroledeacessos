package br.com.acessocontroledeacessos.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cliente {

//    @JsonProperty("cliente_id")
    private int id;
    private String nome;

    public Cliente() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}

package br.com.acessocontroledeacessos.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Porta {

//    @JsonProperty("porta_id")
    private int id;
    private String andar;
    private String sala;

    public Porta() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}

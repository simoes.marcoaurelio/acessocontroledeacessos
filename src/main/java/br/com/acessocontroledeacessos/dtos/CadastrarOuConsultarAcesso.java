package br.com.acessocontroledeacessos.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CadastrarOuConsultarAcesso {

    @JsonProperty("porta_id")
    private int portaId;

    @JsonProperty("cliente_id")
    private int clienteId;

    public CadastrarOuConsultarAcesso() {
    }

    public int getPortaId() {
        return portaId;
    }

    public void setPortaId(int portaId) {
        this.portaId = portaId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}

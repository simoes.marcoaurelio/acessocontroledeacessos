package br.com.acessocontroledeacessos.services;

import br.com.acessocontroledeacessos.clients.ClienteClient;
import br.com.acessocontroledeacessos.clients.PortaClient;
import br.com.acessocontroledeacessos.dtos.Cliente;
import br.com.acessocontroledeacessos.dtos.Porta;
import br.com.acessocontroledeacessos.exceptions.AcessoNegadoException;
import br.com.acessocontroledeacessos.models.Acesso;
import br.com.acessocontroledeacessos.producers.AcessoProducer;
import br.com.acessocontroledeacessos.producers.LogAcesso;
import br.com.acessocontroledeacessos.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private AcessoProducer acessoProducer;

    public Acesso cadastrarAcesso(Acesso acesso) {
        Porta porta = portaClient.consultarPortaPorId(acesso.getPortaId());
        acesso.setPortaId(porta.getId());

        Cliente cliente = clienteClient.consultarClientePorId(acesso.getClienteId());
        acesso.setClienteId(cliente.getId());

        return acessoRepository.save(acesso);
    }

    public void deletarAcesso(int clienteId, int portaId) {
        Acesso acesso = consultarAcesso(clienteId, portaId);
        tratarAcessoNegado(acesso);
        acessoRepository.deleteById(acesso.getId());
    }

    public Acesso consultarAcesso(int clienteId, int portaId) {
        Acesso acesso = acessoRepository.findByClienteIdAndPortaId(clienteId, portaId);
        return acesso;
    }

    public void tratarAcessoNegado(Acesso acesso) {
        if (acesso == null) {
            throw new AcessoNegadoException();
        }
    }

    public LogAcesso registrarLogDeAcesso(Acesso acesso, int clienteId, int portaId) {

        LogAcesso logAcesso = new LogAcesso();

        logAcesso.setDataDoAcesso(LocalDate.now());
        logAcesso.setHoraDoAceeso(LocalTime.now());

        if (acesso != null) {
            logAcesso.setClienteId(acesso.getClienteId());
            logAcesso.setPortaId(acesso.getPortaId());
            logAcesso.setAcessoPermitido(true);
        }
        else {
            logAcesso.setClienteId(clienteId);
            logAcesso.setPortaId(portaId);
            logAcesso.setAcessoPermitido(false);
        }
        acessoProducer.enviarAoKafka(logAcesso);

        return logAcesso;
    }
}
